Requirements:

To fulfill the requirements, you need to build the Docker images as follows:

1)In the "api" folder, execute the following command to create the image:

docker build -t registry.gitlab.com/felipeblujos/project-test:api .

2)In the "sys-stats" folder, execute the following command to create the image:

docker build -t registry.gitlab.com/felipeblujos/project-test:sys-stats .

These steps will allow you to create the necessary Docker images according to the project's specifications. Make sure to run these commands in their respective directories.