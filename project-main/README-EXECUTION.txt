Steps:

1)Install Docker:
Ensure that Docker is installed on your machine.

2)Execute the Processes in README-IMPORTANT.txt:
Follow the processes described in the README-IMPORTANT.txt file located in the same directory as this guide.

3)Run Docker Compose:
Execute the following command to create and start the services using Docker Compose:

docker-compose up --build

4)Test the Routes:
After completing step 3, you can test the following routes in your web browser:

localhost:3000 - Access without using HAProxy.
localhost/sys-stats - Access with HAProxy enabled.
These steps will allow you to set up the environment and test your Docker applications as needed. Make sure to follow the instructions in the README-IMPORTANT.txt file to configure your environment correctly before starting Docker Compose.