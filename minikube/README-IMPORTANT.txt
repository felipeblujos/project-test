Requirements:

Note: React does not communicate via pod names. When running within the Minikube container, some adjustments are required to make the frontend communicate with the backend.

1) You need to reconfigure the React code located in the "src" folder within "sys-stats." Specifically, modify the "app.js" file to make requests to the backend pod's IP. Minikube does not allow the use of static IPs, so you must update the "sys-stats" image with the correct IP in the "src/app.js" file, specifically on LINE 23.
In this case, you can use a publicly available image that I've updated after obtaining the correct IP from the backend. You will need to modify it during the configuration in another location.

Note: When dealing with cloud providers, this wouldn't be an issue as you can manage IPs more flexibly.

HAProxy: If you're using HAProxy, you can switch to NodePort for local solutions or LoadBalancer for cloud providers.
These adjustments are necessary to ensure proper communication between the frontend and backend components, especially when working within Minikube or similar environments.
