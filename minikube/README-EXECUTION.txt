Steps:

1) Ensure Minikube is Installed:
First, make sure you have Minikube installed.

2) Start a Cluster:
Launch a cluster using the following command:

minikube start

3)Check Cluster Status:
Verify that your cluster is up and running by running:

minikube status

4)Apply YAML Files:
Apply the YAML files that define the pods and services. Make sure you're in the same folder as this guide when running the commands:


kubectl apply -f api-deployment.yaml
kubectl apply -f api-service.yaml
kubectl apply -f sys-stats-deployment.yaml
kubectl apply -f sys-stats-service.yaml
kubectl apply -f haproxy-configmap.yaml
kubectl apply -f haproxy-deployment.yaml
kubectl apply -f haproxy-service.yaml

5)Read and Follow README-IMPORTANT:

Review and follow the instructions provided in the README-IMPORTANT file located in the same folder as this guide.

6)Verify Service Status: 

Check if the services are running correctly:

kubectl get pods
kubectl get services


7) Obtain Frontend Port and Route:
To access the frontend, obtain the port and route it is exposing. Note that when using a load balancer in a cloud provider, you may not need to use NodePort, and external routing can be predefined. However, the Minikube service exposure does not allow setting the port when leaving the cluster. Obtain the frontend URL with:

minikube service haproxy-service --url

These steps will help you set up and verify your Minikube environment along with the necessary services. Make sure to follow each step carefully to ensure a successful setup.



