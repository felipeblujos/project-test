provider "aws" {
  region = "us-east-1"  # REGION DESIRED
}

# VPC
resource "aws_vpc" "project" {
  cidr_block = "10.0.0.0/16"
}

# SUB-NETWORK
resource "aws_subnet" "subnet_a" {
  cidr_block        = "10.0.1.0/24"
  vpc_id            = aws_vpc.project.id
  availability_zone = "us-east-1a"
}

# SECURITY GROUPS 
resource "aws_security_group" "ecs_security_group" {
  name        = "ecs_security_group"
  description = "ECS security group"
  vpc_id      = aws_vpc.project.id
}

# SECURITY GROUPS 
resource "aws_security_group_rule" "ecs_ingress" {
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ecs_security_group.id
}

# CLUSTER
resource "aws_ecs_cluster" "test-project" {
  name = "test-project"  #NAME CLUSTER
}

#IAM
resource "aws_iam_role" "ecs_execution_role" {
  name = "ecs_execution_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Action = "sts:AssumeRole",
      Effect = "Allow",
      Principal = {
        Service = "ecs-tasks.amazonaws.com",
      },
    }],
  })
}

# IMAGES FOR PODS
variable "image_to_service_name" {
  type = map(string)
  default = {
    "registry.gitlab.com/felipeblujos/project-test:api" = "main-api-backend",
    "registry.gitlab.com/felipeblujos/project-test:sys-stats" = "sys-stats-frontend",
  }
}

# TASKS
resource "aws_ecs_task_definition" "haproxy" {
  family                  = "haproxy"
  network_mode            = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  execution_role_arn      = aws_iam_role.ecs_execution_role.arn

  container_definitions = jsonencode([{
    name  = "haproxy",
    image = "haproxy:latest",  # LATEST HAPROXY IMAGE
    portMappings = [{
      containerPort = 80,
      hostPort      = 80,
    }],
  }])

  #THIS ENSURES THAT THE TASK DEFINITION IS UPDATED BEFORE CREATING A NEW REVISION.
  lifecycle {
    create_before_destroy = true
  }
  cpu    = 256  # CPU DESIRED
  memory = 512  # MEMORY DESIRED
}

# SERVICE
resource "aws_ecs_service" "haproxy" {
  name            = "haproxy-service"
  cluster         = aws_ecs_cluster.test-project.id
  task_definition = aws_ecs_task_definition.haproxy.arn
  launch_type     = "FARGATE"

  network_configuration {
    subnets = [aws_subnet.subnet_a.id]  # Anexe a sub-rede criada
    security_groups = [aws_security_group.ecs_security_group.id]  # Use o grupo de segurança
  }
}

# SERVICE
resource "aws_ecs_task_definition" "existing_images" {
  count                   = 2
  family                  = element([
    "main-api",
    "sys-stats-frontend",
  ], count.index)
  network_mode            = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  execution_role_arn      = aws_iam_role.ecs_execution_role.arn

  container_definitions = jsonencode([{
    name  = lookup(var.image_to_service_name, element([
      "registry.gitlab.com/felipeblujos/project-test:api",
      "registry.gitlab.com/felipeblujos/project-test:sys-stats",
    ], count.index)),
    image = element([
      "registry.gitlab.com/felipeblujos/project-test:api",
      "registry.gitlab.com/felipeblujos/project-test:sys-stats",
    ], count.index),
    portMappings = [{
      containerPort = count.index == 0 ? 5000 : 80,  # PORT 5000 FOR main-api AND 80 FOR sys-stats-frontend
      hostPort      = count.index == 0 ? 5000 : 80,
    }],
  }])

  #   # THIS ENSURES THAT THE TASK DEFINITION IS UPDATED BEFORE CREATING A NEW REVISION
  lifecycle {
    create_before_destroy = true
  }
  cpu    = 256  # CPU DESIRED
  memory = 512  # MEMORY DESIRED
}

# SERVICE
resource "aws_ecs_service" "existing_images" {
  count           = 2
  name            = lookup(var.image_to_service_name, element([
    "registry.gitlab.com/felipeblujos/project-test:api",
    "registry.gitlab.com/felipeblujos/project-test:sys-stats",
  ], count.index))
  cluster         = aws_ecs_cluster.test-project.id
  task_definition = aws_ecs_task_definition.existing_images[count.index].arn
  launch_type     = "FARGATE"

  network_configuration {
    subnets = [aws_subnet.subnet_a.id]  # ATTACH SUB-NETWORK
    security_groups = [aws_security_group.ecs_security_group.id]  # USE SECURITY GROUP
  }
}

